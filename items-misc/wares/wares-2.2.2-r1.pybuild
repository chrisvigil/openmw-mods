# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Wares"
    DESC = "Wares gives traders more Wares (duh)"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/49205"
    KEYWORDS = "~openmw"
    LICENSE = "mw attribution-derivation-nc"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        oaab? (
            base/oaab-data
        )
        tr? (
            landmasses/tamriel-rebuilt
        )
        virtual/merged-plugin
    """
    TIER = 1
    IUSE = "tr oaab"
    NEXUS_SRC_URI = """
        https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025384 -> Wares-49205-2-2-2-1623243803.7z
        oaab? (
            https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025183 -> OAAB_Wares-49205-1-1-1-1622539225.7z
        )
    """
    # These could probably be separate packages, as they just seem to be a collection of mods which use Wares, not related to wares itself
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000024897 -> Andoreth_Potted_Plants_for_Wares-49205-1-1-1621630486.zip
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025179 -> Andoreth_Tot_Shoppe-49205-1-1-1-1622539031.zip
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025181 -> Trancemaster's_Spell_Books-49205-1-1-1-1622539126.7z
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025182 -> Melchior_Licj_Robes-49205-1-1-1-1622539187.7z
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025184 -> Mandamus_Pirate_outfit-49205-1-0-1-1622539421.7z
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025185 -> Remiros_Hunters_Mark-49205-1-0-1-1622539465.7z
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025186 -> Triss_Wares-49205-1-0-2-1622539616.zip
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025205 -> Wulfgar_Rings-49205-1-0-1622543859.7z
    # https://nexusmods.com/morrowind/mods/49205?tab=files&file_id=1000025385 -> Danke_armor_wares-49205-1-0-1623243987.7z
    # See nexus page
    DATA_OVERRIDES = "!!base/tomb-of-the-snow-prince"
    INSTALL_DIRS = [
        InstallDir(
            "00 Master file",
            S="Wares-49205-2-2-2-1623243803",
            PLUGINS=[File("Wares-base.esm")],
        ),
        InstallDir(
            "01 TR",
            S="Wares-49205-2-2-2-1623243803",
            PLUGINS=[File("Wares_TR.ESP")],
            REQUIRED_USE="tr",
        ),
        # From README: use this OR Wares-TR
        InstallDir(
            "01 Vvardenfell",
            S="Wares-49205-2-2-2-1623243803",
            PLUGINS=[File("Wares_Vvardenfell.ESP")],
            REQUIRED_USE="!tr",
        ),
        # Doubles the amount of items traders have
        # InstallDir(
        #     "03 MOAR",
        #     S="Wares-49205-2-2-2-1623243803",
        #     PLUGINS=[File("Wares-MOAR.ESP")],
        # ),
        # Provides TR items without the landmass.
        # E.g. you could use this, with a dependency on tamriel-data,
        # but no dependency on tamriel-rebuilt
        # InstallDir(
        #    "04 TR DATA only",
        #    S="Wares-49205-2-2-2-1623243803",
        #    PLUGINS=[File("Wares-TR_DATA.esp")],
        #    REQUIRED_USE="tr",
        # ),
        # InstallDir(
        #    ".",
        #    S="Andoreth_Potted_Plants_for_Wares-49205-1-1-1621630486",
        #    PLUGINS=[File("Potted Plants - Wares.esp"), File("Wares-base.esm")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Andoreth_Tot_Shoppe-49205-1-1-1-1622539031",
        #    PLUGINS=[File("Toy Shoppe - Wares.esp")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Danke_armor_wares-49205-1-0-1623243987",
        #    PLUGINS=[File("danke_armors.esp")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Mandamus_Pirate_outfit-49205-1-0-1-1622539421",
        #    PLUGINS=[File("Pirate Outfit by Mandamus.ESP")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Melchior_Licj_Robes-49205-1-1-1-1622539187",
        #    PLUGINS=[File("md_liche_robe.ESP")],
        # ),
        InstallDir(
            ".",
            S="OAAB_Wares-49205-1-1-1-1622539225",
            PLUGINS=[File("OAAB for Wares.ESP")],
            REQUIRED_USE="oaab",
        ),
        # InstallDir(
        #    ".",
        #    S="Remiros_Hunters_Mark-49205-1-0-1-1622539465",
        #    PLUGINS=[
        #        File("Hunter's Mark - Wares.ESP"),
        #        File("Hunter's Mark - A Marksman Mod.ESP"),
        #    ],
        # ),
        # InstallDir(
        #    ".",
        #    S="Trancemaster's_Spell_Books-49205-1-1-1-1622539126",
        #    PLUGINS=[File("Jobashas Spell Tomes.ESP")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Triss_Wares-49205-1-0-2-1622539616",
        #    PLUGINS=[File("Triss Multicolor.esp")],
        # ),
        # InstallDir(
        #    ".",
        #    S="Wulfgar_Rings-49205-1-0-1622543859",
        #    PLUGINS=[File("LotsOfRingsPlus.esp")],
        # ),
    ]
