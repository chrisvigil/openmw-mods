# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import shutil
from common.system import System
from pybuild.info import PN


class Package(System):
    NAME = "convert"
    DESC = "A command line tool for manipulating images that is part of ImageMagick"
    HOMEPAGE = "https://imagemagick.org"
    RELEASE_PAGE = "https://imagemagick.org/script/download.php"
    KEYWORDS = "openmw tes3mp"

    def pkg_pretend(self):
        exec_path = shutil.which("magick")

        if not exec_path:
            string = (
                'Unable to find executable "magick"\n'
                "Please download and install it using your package manager\n"
                "or from the following location(s):\n"
            )
            string += f"    {self.RELEASE_PAGE}\n"

            string += (
                """Imagemagick 6 might be installed, but it won't be detected or """
                """used as it doesn't provide the "magick" command.\n"""
                "Imagemagick 7 or newer is required.\n"
            )

            self.warn(string)

    def get_version(self):
        out = self.execute("magick convert --version", pipe_output=True)
        for line in out.splitlines():
            if line.startswith("Version: "):
                return line.split()[2].replace("-", ".")

        raise Exception(f"Could not determine version of {PN}!")
