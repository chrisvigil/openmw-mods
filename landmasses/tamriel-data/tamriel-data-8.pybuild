# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import DOWNLOAD_DIR, File, InstallDir, Pybuild1
from pybuild.info import PV

VANILLA_FILE = f"Tamriel_Data_v{PV}"
HD_FILE = f"Tamriel_Data_v{PV}_-_HD"


class Package(Pybuild1):
    NAME = "Tamriel Data"
    DESC = "Adds game data files required by several landmass addition mods"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org
        https://www.nexusmods.com/morrowind/mods/44537
    """
    # License in 8 is the same as in 7.1
    LICENSE = "tamriel-data-7.1"
    # weapon-sheathing-tr is included
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        !!gameplay-weapons/weapon-sheathing-tr
    """
    KEYWORDS = "openmw"
    SRC_URI = f"""
        texture_size_512? ( {VANILLA_FILE}.7z )
        devtools? ( Tamriel_Data_Legacy-44537-06-01.7z )
        texture_size_1024? ( {HD_FILE}.7z )
    """
    IUSE = "devtools"
    RESTRICT = "fetch"
    TIER = 2

    TEXTURE_SIZES = "512 1024"
    INSTALL_DIRS = [
        # InstallDir(".", S="Tamriel_Data_Animkit_Meshes-44537-1-0a"),
        InstallDir(
            "00 Core",
            ARCHIVES=[File("TR_Data.bsa"), File("PT_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=VANILLA_FILE,
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir(
            "00 Core",
            ARCHIVES=[File("TR_Data.bsa"), File("PT_Data.bsa")],
            PLUGINS=[File("Tamriel_Data.esm")],
            S=HD_FILE,
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            ".",
            PLUGINS=[
                # Note that Tamriel_Data_Legacy should not be enabled in game.
                # This option is included as a development tool.
                # File("Tamriel_Data_Legacy.esm")
            ],
            S="Tamriel_Data_Legacy-44537-06-01",
            REQUIRED_USE="devtools",
        ),
    ]

    def pkg_nofetch(self):
        print("Please download the following file(s) from the url(s) at the bottom")
        print("before continuing and move them to the download directory:")
        print(f"  {DOWNLOAD_DIR}")
        print()
        for source in self.A:
            print(f"  {source}")
        print()
        if "devtools" in self.USE:
            print("  https://www.nexusmods.com/morrowind/mods/44537/?tab=files")
        if "texture_size_512" in self.USE:
            print(
                "  https://drive.google.com/open?id=1r1mxp3vXi6wHvm5NQrzp3yr3lRFEcem5"
            )
        elif "texture_size_1024" in self.USE:
            print(
                "  https://drive.google.com/open?id=1BRAV4ofQ8_DO4fh3BjLFhDFH32EjZz5h"
            )
