# Copyright 2019-2020 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from pybuild import File, InstallDir, Pybuild1
from pybuild.info import P, PN


class Package(Pybuild1):
    NAME = "Tamriel Rebuilt"
    DESC = "A project that aims to fill in the mainland of Morrowind"
    HOMEPAGE = """
        http://www.tamriel-rebuilt.org/
        http://www.nexusmods.com/morrowind/mods/42145
    """
    # No changes (except copyright date) since 20.02
    LICENSE = "tamriel-rebuilt-20.02"
    RESTRICT = "mirror"
    # A list of landmass conflicts can be found in the README
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        >=landmasses/tamriel-data-8
        music? ( media-audio/tamriel-rebuilt-music )
        preview? ( !!landmasses/wizards-islands )
    """
    OLD_P = f"{PN}-21.1"
    KEYWORDS = "openmw"
    SRC_URI = f"""
        https://drive.google.com/uc?id=17gqe2zdok6Or7-weAShTxyreHHRXwpyx&e=download
        -> {OLD_P}.7z
        https://drive.google.com/uc?id=1cgWUp0UPgyO1emamxl5M85bhHzcfTj65&e=download
        -> {P}.7z
    """
    IUSE = "preview travels +music"
    TIER = 2

    INSTALL_DIRS = [
        InstallDir("00 Core", PLUGINS=[File("TR_Mainland.esm")], S=OLD_P),
        InstallDir(
            "01 Faction Integration", PLUGINS=[File("TR_Factions.esp")], S=OLD_P
        ),
        InstallDir(
            "02 Preview Content",
            PLUGINS=[File("TR_Preview.esp")],
            REQUIRED_USE="preview",
            S=OLD_P,
        ),
        InstallDir(
            "03 Travel Network for Core and Vvardenfell",
            PLUGINS=[File("TR_Travels.esp")],
            REQUIRED_USE="travels !preview",
            S=OLD_P,
        ),
        InstallDir(
            "03 Travel Network for Core, Preview, and Vvardenfell",
            PLUGINS=[File("TR_Travels_(Preview_and_Mainland).esp")],
            REQUIRED_USE="preview travels",
            S=OLD_P,
        ),
        # TODO: Requires Abot's Travels
        # InstallDir(
        #     "04 Patch for Abot's Travels mods",
        #     PLUGINS=[
        #         File("TR_OldTravels.esp", ),
        #     ],
        #     REQUIRED_USE="old-travels"
        #     S=P
        # ),
        InstallDir("00 Core", PLUGINS=[File("TR_Mainland_2101_hotfix.ESP")], S=P),
        InstallDir("01 Faction Integration", PLUGINS=[File("TR_Factions.esp")], S=P),
    ]
