# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from pybuild import File, InstallDir, Pybuild1

from common.nexus import NexusMod


class Package(NexusMod, Pybuild1):
    NAME = "Remiros' Groundcover"
    DESC = "Adds groundcover to almost all regions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46733"
    KEYWORDS = "openmw"
    LICENSE = "attribution-nc"
    RDEPEND = """
        modules/openmw-config[grass]
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-rebuilt[preview?] )
    """
    TEXTURE_SIZES = "512 1024"
    MAIN_FILE = "Remiros'_Groundcover-46733-2-4-1621097530"
    NEXUS_SRC_URI = f"""
        https://www.nexusmods.com/morrowind/mods/46733?tab=files&file_id=1000024695
        -> {MAIN_FILE}.7z
    """
    IUSE = "tr preview +solstheim minimal mushrooms thick-grass"
    # With minimal enabled, the only things provided are the assets,
    # which are needed for base/tomb-of-the-snow-prince[grass]
    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            GROUNDCOVER=[
                File("Rem_AC.esp", REQUIRED_USE="!minimal"),
                File("Rem_AI.esp", REQUIRED_USE="!minimal"),
                File("Rem_AL.esp", REQUIRED_USE="!minimal"),
                File("Rem_BC.esp", REQUIRED_USE="!minimal"),
                File("Rem_GL.esp", REQUIRED_USE="!minimal"),
                File("Rem_Solstheim.esp", REQUIRED_USE="solstheim !minimal"),
                File("Rem_WG.esp", REQUIRED_USE="!minimal"),
            ],
            S=MAIN_FILE,
        ),
        InstallDir(
            "01a No Mushrooms",
            REQUIRED_USE="!mushrooms",
            S=MAIN_FILE,
        ),
        InstallDir(
            "01b Thicker Grass",
            REQUIRED_USE="thick-grass",
            S=MAIN_FILE,
        ),
        InstallDir(
            "02 Vanilla Resolution Textures",
            REQUIRED_USE="texture_size_512",
            S=MAIN_FILE,
        ),
        InstallDir(
            "03 TR Plugins",
            GROUNDCOVER=[
                File("Rem_TR_AI.esp"),
                File("Rem_TR_BC.esp"),
                File("Rem_TR_RR.esp"),
                File("Rem_TR_AC.esp"),
                File("Rem_TR_AT.esp"),
                File("Rem_TR_GL.esp"),
                File("Rem_TR_WG.esp"),
            ],
            REQUIRED_USE="tr !minimal",
            S=MAIN_FILE,
        ),
        InstallDir(
            "04 TR Preview Plugins",
            GROUNDCOVER=[
                File("Rem_TRp_AI.esp"),
                File("Rem_TRp_AL.esp"),
                File("Rem_TRp_AT.esp"),
                File("Rem_TRp_BC.esp"),
                File("Rem_TRp_GL.esp"),
                File("Rem_TRp_GM.esp"),
                File("Rem_TRp_RR.esp"),
                File("Rem_TRp_Sol.esp"),
                File("Rem_TRp_TV.esp"),
                File("Rem_TRp_WG.esp"),
            ],
            REQUIRED_USE="preview !minimal",
            S=MAIN_FILE,
        ),
        # InstallDir(
        #     "05a Legend of Chemua",
        #     PLUGINS=[File("Rem_LoC.esp")],
        # ),
        # InstallDir(
        #     "05b Legend of Chemua Moved",
        #     PLUGINS=[File("Rem_LoCM.esp")],
        # ),
    ]
